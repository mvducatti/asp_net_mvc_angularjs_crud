﻿//Serviço para pegar a 'data' do usuário mvc controller
myapp.service('employeeService', function ($http) {

    //***********************************************************************//
    //***********************************************************************//
    //*************************** USERS *************************************//
    //***********************************************************************//
    //***********************************************************************//

    //get User
    this.getAllEmployees = function () {
        return $http.get("/User/GetUsers");
    }

    //add User
    this.save = function (user) {
        var request = $http({
            method: 'post',
            url: '/User/Insert',
            data: user
        });

        return request;
    }

    //update User
    this.update = function (user) {
        var updaterequest = $http({
            method: 'post',
            url: '/User/Update',
            data: user
        });

        return updaterequest;
    }

    //delete User
    this.delete = function (updatedUserId) {
        return $http.post('/User/Delete/' + updatedUserId);
    }

    //************************************************************************//
    //************************************************************************//
    //*************************** PHONES *************************************//
    //************************************************************************//
    //************************************************************************//

    //get UsePhoner
    this.getAllPhones = function (updatedUserId) {
        return $http.get('/Phone/GetPhones/' + updatedUserId);
    }

    //add Phone
    this.savePhone = function (phone) {
        var request = $http({
            method: 'post',
            url: '/Phone/Insert',
            data: phone
        });

        return request;
    }

    //delete Phone
    this.deletePhone = function (phoneid) {
        return $http.post('/Phone/Delete/' + phoneid);
    }

    //*************************************************************************//
    //*************************************************************************//
    //*************************** ADDRESS *************************************//
    //*************************************************************************//
    //*************************************************************************//

    //get Address
    this.getAllAddress = function (updatedUserId) {
        return $http.get('/Address/GetAddress/' + updatedUserId);
    }

    //add Address
    this.saveAddress = function (address) {
        var request = $http({
            method: 'post',
            url: '/Address/Insert',
            data: address
        });

        return request;
    }

    //delete Address
    this.deleteAddress = function (addressid) {
        return $http.post('/Address/Delete/' + addressid);
    }

    //update Address
    this.updateAddress = function (address) {
        var updateAddressrequest = $http({
            method: 'post',
            url: '/Address/Update',
            data: address
        });

        return updateAddressrequest;
    }
});