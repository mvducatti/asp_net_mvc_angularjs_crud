﻿//Criando o controlador do usuario
myapp.controller('employee-controller', function ($scope, employeeService) {


    //***********************************************************************//
    //***********************************************************************//
    //*************************** USERS *************************************//
    //***********************************************************************//
    //***********************************************************************//

    //Carrega todos os dados gravados quando a página for recarregada
    loadEmployees();

    //GET users
    function loadEmployees() {
        var employeeRecords = employeeService.getAllEmployees();
        employeeRecords.then(function (d) {    
            $scope.Users = d.data;
        },
        function () {
            alert("Error occured while fetching users list...");
        });
    }

    //INSERT user
    $scope.save = function ()
    {
        var user = {
            userid: $scope.userid,
            username: $scope.username,
            birthdate: $scope.birthdate,
            cpf: $scope.cpf,
        };

        var saverecords = employeeService.save(user);

        saverecords.then(function (d) {
            if (d.data.success === true) {
                loadEmployees();
                alert("Registro adicionado com sucesso");
                $scope.resetSave();
            }
            else { alert("Registro não adicionado"); }
        },
            function () {
                alert("Erro tentando adicinar registro. Verifique se todos os campos foram preenchidos.");
            });
    }

    //limpar campos após inserir dados na db
    $scope.resetSave = function () {
        $scope.username = '';
        $scope.birthdate = '';
        $scope.cpf = '';
    }

    //Verifica user pelo ID
    $scope.updatebyId = function (user) {
        $scope.updatedUserId = user.userid;
        $scope.updatedName = user.username;
        $scope.updatedBirthDate = user.birthdate;
        $scope.updatedCpf = user.cpf;
    }

    //UPDATE user
    $scope.update = function (user)
    {
        var user = {
            userid: $scope.updatedUserId,
            username: $scope.updatedName,
            birthdate: $scope.updatedBirthDate,
            cpf: $scope.updatedCpf,
        };

        var updaterecords = employeeService.update(user);
        updaterecords.then(function (d) {
            if (d.data.success === true) {
                alert("Registro atualizado com sucesso");
                loadEmployees();
                $scope.resetUpdate();
            }
            else {
                alert("Registro não foi atualizado");
            }
        },
            function () {
                alert("Ocorreu um erro durante a atualização do registro");
            });
    }

    //limpar campos após atualização
    $scope.resetUpdate = function ()
    {
        $scope.updatedUserId = '';
        $scope.updatedName = '';
        $scope.updatedBirthDate = '';
        $scope.updatedCpf = '';
    }

    //DELETE user
    $scope.delete = function (updatedUserId)
    {
        var deleteUser = employeeService.delete($scope.updatedUserId);
        deleteUser.then(function (d) {
            if (d.data.success === true) {
                loadEmployees();
            }
            else {
                alert("Registro não foi deletado");
            }
        }, function () {
            alert("Certifique-se de que todos os telefones e endereços foram deletados antes de excluir o usuário");
        });
    }

    //pegar dados para confirmar exclusão
    $scope.deletebyId = function (user)
    {
        $scope.updatedUserId = user.userid;
        $scope.updatedName = user.username;
        $scope.updatedBirthDate = user.birthdate;
        $scope.updatedCpf = user.cpf;
    }


    //************************************************************************//
    //************************************************************************//
    //*************************** PHONES *************************************//
    //************************************************************************//
    //************************************************************************//

    //GET phone
    $scope.loadPhones = function loadPhones(updatedUserId) {
        var phoneRecords = employeeService.getAllPhones($scope.updatedUserId);
        phoneRecords.then(function (d) {
            $scope.Phones = d.data;
        },
            function () {
                alert("Error occured while fetching phones list...");
            });
    }

    //INSERT phone
    $scope.savePhones = function (phone, updatedUserId) {

        var phone = {
            phoneid: $scope.phoneid,
            phone1: $scope.phone1,
            userfk: $scope.updatedUserId,
        };

        var savePhonerecords = employeeService.savePhone(phone);

        savePhonerecords.then(function (d) {
            if (d.data.success === true) {
                $scope.resetSavePhones();
                $scope.loadPhones();
            }
            else { alert("Telefone não adicionado"); }
        },
            function () {
                alert("Erro tentando adicinar telefone. Verifique se todos os campos foram preenchidos.");
            });
    }

    //limpar campos após inserir dados na db
    $scope.resetSavePhones = function () {
        $scope.phone1 = '';
    }

    //DELETE Phone
    $scope.deletePhones = function (updatedPhoneId) {
        var deletePhone = employeeService.deletePhone($scope.updatedPhoneId);
        deletePhone.then(function (d) {
            if (d.data.success === true) {
                $scope.loadPhones();
            }
            else {
                alert("Registro não foi deletado");
            }
        }, function () {
            alert("Ocorreu um erro ao tentar excluir o telefone " + updatedPhoneId);
        });
    }

    //pegar dados para confirmar exclusão
    $scope.deletePhonebyId = function (phone) {
        $scope.updatedPhoneId = phone.phoneid;
        $scope.updatedPhone = phone.phone1;
        $scope.updatedUSerfk = phone.userfk;
    }

    //*************************************************************************//
    //*************************************************************************//
    //*************************** ADDRESS *************************************//
    //*************************************************************************//
    //*************************************************************************//

    //GET address
    $scope.loadAddress = function loadAddress(updatedUserId) {
        var addressRecords = employeeService.getAllAddress($scope.updatedUserId);
        addressRecords.then(function (d) {
            $scope.Addresses = d.data;
        },
            function () {
                alert("Error occured while fetching address list...");
            });
    }

    //INSERT address
    $scope.saveAddress = function (address, updatedUserId) {

        var address = {
            addressid: $scope.addressid,
            street: $scope.street,
            number: $scope.number,
            neighborhood: $scope.neighborhood,
            cep: $scope.cep,
            city: $scope.city,
            state: $scope.state,
            userfk: $scope.updatedUserId,
        };

        var saveAdsRecord = employeeService.saveAddress(address);

        saveAdsRecord.then(function (d) {
            if (d.data.success === true) {
                $scope.resetSaveAddress();
                $scope.loadAddress();
            }
            else { alert("Endereço não adicionado"); }
        },
            function () {
                alert("Erro tentando adicinar endereço. Verifique se todos os campos foram preenchidos.");
            });
    }

    //limpar campos após inserir dados na db
    $scope.resetSaveAddress = function () {
        $scope.street = '';
        $scope.number = '';
        $scope.neighborhood = '';
        $scope.cep = '';
        $scope.city = '';
        $scope.state = '';
    }

    //DELETE address
    $scope.delAddress = function (updatedAddressId) {
        var dropAddress = employeeService.deleteAddress($scope.updatedAddressId);
        dropAddress.then(function (d) {
            if (d.data.success === true) {
                $scope.loadAddress();
            }
            else {
                alert("Registro não foi deletado");
            }
        }, function () {
            alert("Ocorreu um erro ao tentar excluir o endereco " + updatedAddressId);
        });
    }

    //pegar dados para confirmar exclusão
    $scope.deleteAddressbyId = function (address) {
        $scope.updatedAddressId = address.addressid;
    }

    //Verifica user pelo ID
    $scope.updateAddressbyId = function (address) {
        $scope.updatedAddressId = address.addressid;
        $scope.updatedStreet = address.street;
        $scope.updatedNumber = address.number;
        $scope.updatedNeighborhood = address.neighborhood;
        $scope.updatedCep = address.cep;
        $scope.updatedCity = address.city;
        $scope.updatedState = address.state;
    }

    //UPDATE address
    $scope.updateAddress = function (address) {

        var address = {
            addressid: $scope.updatedAddressId,
            street: $scope.updatedStreet,
            number: $scope.updatedNumber,
            neighborhood: $scope.updatedNeighborhood,
            cep: $scope.updatedCep,
            city: $scope.updatedCity,
            state: $scope.updatedState,
        };

        var updateaddrsReg = employeeService.updateAddress(address);
        updateaddrsReg.then(function (d) {
            if (d.data.success === true) {
                alert("Registro atualizado com sucesso");
                $scope.loadAddress();
                $scope.resetAddressUpdate();s
            }
            else {
                alert("Registro não foi atualizado");
            }
        },
            function () {
                alert("Ocorreu um erro durante a atualização do registro");
            });
    }

    //limpar scope após atualização
    $scope.resetAddressUpdate = function () {
        $scope.updatedAddressId = '';
        $scope.updatedStreet = '';
        $scope.updatedNumber = '';
        $scope.updatedNeighborhood = '';
        $scope.updatedCep = '';
        $scope.updatedCity = '';
        $scope.updatedState = '';
    }

});