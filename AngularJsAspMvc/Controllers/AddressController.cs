﻿using AngularJsAspMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularJsAspMvc.Controllers
{
    public class AddressController : Controller
    {
        // GET: Address/GetPhones
        public JsonResult GetAddress(int id)
        {
            using (CadDBEF db = new CadDBEF())
            {
                //Desbilitando LazyLoading devido a problemas de carregamento de dados
                db.Configuration.LazyLoadingEnabled = false;

                List<Address> list = db.Address.Where(x => x.userfk == id).ToList();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        //POST Address/Insert
        [HttpPost]
        public JsonResult Insert(Address address)
        {
            if (address != null)
            {
                using (CadDBEF db = new CadDBEF())
                {
                    db.Address.Add(address);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
            }
            return Json(new { success = false });
        }

        //POST Address/Update
        [HttpPost]
        public JsonResult Update(Address address)
        {
            using (CadDBEF db = new CadDBEF())
            {
                var updatedAddress = db.Address.Find(address.addressid);

                if (updatedAddress == null)
                {
                    return Json(new { success = false });
                }
                else
                {
                    updatedAddress.street = address.street;
                    updatedAddress.number = address.number;
                    updatedAddress.neighborhood = address.neighborhood;
                    updatedAddress.cep = address.cep;
                    updatedAddress.city = address.city;
                    updatedAddress.state = address.state;

                    db.SaveChanges();
                    return Json(new { success = true });
                }
            }
        }

        //POST Address/Delete/1
        [HttpPost]
        public JsonResult Delete(int id)
        {
            using (CadDBEF db = new CadDBEF())
            {
                var address = db.Address.Find(id);

                if (address == null)
                {
                    return Json(new { success = false });
                }

                db.Address.Remove(address);
                db.SaveChanges();

                return Json(new { success = true });
            }
        }
    }
}