﻿using AngularJsAspMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularJsAspMvc.Controllers
{
    public class PhoneController : Controller
    {
        // GET: Phone/GetPhones
        public JsonResult GetPhones(int id)
        {
            using (CadDBEF db = new CadDBEF())
            {
                //Desbilitando LazyLoading devido a problemas de carregamento de dados
                db.Configuration.LazyLoadingEnabled = false;

                List<Phone> list = db.Phone.Where(x=> x.userfk == id).ToList();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
        }

        //POST User/Insert
        [HttpPost]
        public JsonResult Insert(Phone phone)
        {
            if (phone != null)
            {
                using (CadDBEF db = new CadDBEF())
                {
                    db.Phone.Add(phone);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
            }
            return Json(new { success = false });
        }

        //POST Phone/Delete/1
        [HttpPost]
        public JsonResult Delete(int id)
        {
            using (CadDBEF db = new CadDBEF())
            {
                var phone = db.Phone.Find(id);

                if (phone == null)
                {
                    return Json(new { success = false });
                }

                db.Phone.Remove(phone);
                db.SaveChanges();

                return Json(new { success = true });
            }
        }
    }
}