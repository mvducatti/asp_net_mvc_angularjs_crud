﻿using AngularJsAspMvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AngularJsAspMvc.Controllers
{
    public class UserController : Controller
    {
        // GET: User/GetEmployee
        public JsonResult GetUsers()
        {
            using (CadDBEF db = new CadDBEF())
            {
                //Desbilitando LazyLoading devido a problemas de carregamento de dados
                db.Configuration.LazyLoadingEnabled = false;
                List<User> empList = db.User.ToList();
                return Json(empList, JsonRequestBehavior.AllowGet);
            }
        }

        //POST User/Insert
        [HttpPost]
        public JsonResult Insert(User user)
        {
            if (user != null)
            {
                using (CadDBEF db = new CadDBEF())
                {
                    db.User.Add(user);
                    db.SaveChanges();
                    return Json(new { success = true });
                }
            }
                return Json(new { success = false });
        }

        //POST User/Update
        [HttpPost]
        public JsonResult Update(User user)
        {
            using (CadDBEF db = new CadDBEF())
            {
                var updatedUser = db.User.Find(user.userid);

                if (updatedUser == null)
                {
                    return Json(new { success = false });
                }
                else
                {
                    updatedUser.username = user.username;
                    updatedUser.birthdate = user.birthdate;
                    updatedUser.cpf = user.cpf;

                    db.SaveChanges();
                    return Json(new { success = true });
                }
            }
        }

        //POST User/Delete/1
        [HttpPost]
        public JsonResult Delete(int id)
        {
            using (CadDBEF db = new CadDBEF())
            {
                var user = db.User.Find(id);

                if (user == null)
                {
                    return Json(new { success = false });
                }

                db.User.Remove(user);
                db.SaveChanges();

                return Json(new { success = true });
            }
        }
    }
}