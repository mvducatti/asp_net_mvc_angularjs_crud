﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJsAspMvc.Models
{
    public class UserPhone
    {
        public int phoneid { get; set; }
        public string phone1 { get; set; }
        public Nullable<int> userfk { get; set; }
    }
}